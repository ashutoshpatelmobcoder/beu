export default {
  api: {
    url: 'https://107.23.104.106:4009/re4m/api/v1/',
    mode: 'cors'
  },
  socket: 'https://107.23.104.106:4010/re4m'
}
