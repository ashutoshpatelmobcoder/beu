
export default {
  api: {
    url: 'https://devapi.re4m.io/re4m/api/v1/',
    mode: 'cors'
  },
  socket: 'https://devsocket.re4m.io/re4m'
}
