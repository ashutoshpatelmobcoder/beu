
export default {
    api: {
        url: 'http://localhost:4009/re4m/api/v1/',
        mode: 'cors'
    },
    socket: 'http://localhost:4010/re4m'
}
