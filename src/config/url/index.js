import _ from "lodash";

import DevSetting from './development';
import ProdSetting from './production';
import StagSetting from './staging';
import api from './localhost';

let defaultSetting = {
  api: {
    url: 'http://localhost:4009/re4m/api/v1/',
    mode: 'cors'
  },
  socket: 'http://localhost:4010/re4m'
}

let siteSetting = defaultSetting;
//console.log(process.env.REACT_APP_ENV, "d5rftgyhujik");
switch (process.env.REACT_APP_ENV) {
  case "prod":
  case "production":
    siteSetting = _.extend(defaultSetting, ProdSetting);
    break;
  case "stag":
  case "staging":
    siteSetting = _.extend(defaultSetting, StagSetting);
    break;
  case "dev":
  case "development":
    siteSetting = _.extend(defaultSetting, DevSetting);
    break;
  case "local":
  case "localhost":
    siteSetting = _.extend(defaultSetting, api);
    break;
  default:
    siteSetting = _.extend(defaultSetting, api);
}

export default siteSetting;
