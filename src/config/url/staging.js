export default {
  api: {
    url: 'https://stagapi.re4m.io/re4m/api/v1/',
    mode: 'cors'
  },
  socket: 'https://stagsocket.re4m.io/re4m'
}
