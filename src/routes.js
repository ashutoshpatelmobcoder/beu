import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { createBrowserHistory } from 'history';

import { alertActions } from './_actions';
import {PrivateRoute} from "./routing/PrivateRoute";
import Home from "./component/Home";
import Dashboard from "./component/Dashboard";

import './App.css';
import Login from "./component/Login";

export const history = createBrowserHistory();



class Routes extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }
  render() {
    return (
        <Router history={history}>
          <div>
            <Route exact path="/" component={Home} />
            <PrivateRoute path='/login' component={Login} />
            <PrivateRoute path='/dashboard' component={Dashboard} />
          </div>
        </Router>
    );
  }
}



function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedApp = connect(mapStateToProps)(Routes);
export { connectedApp as Routes };





