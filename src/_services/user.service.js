import axios from 'axios';
import API from "../Config/API";



export const userService = {

    logout,

};



function logout(params) {
    // remove user from local storage to log user out
    localStorage.clear();
    return axios.post(API.LOG_OUT, params);

}

